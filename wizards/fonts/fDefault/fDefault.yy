{
    "id": "5a17f584-a831-4061-86ca-e44535d28209",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fDefault",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Open Sans",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "15871d35-05ea-4c2d-98d3-105bcf2df4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a31892e1-e295-451d-9364-850a0b7a7b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6170774d-3b34-4ac0-a0a0-361a10c47674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 103,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "42a9096e-2318-4c65-a3f9-259fa2db7c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ef5dab3a-1f2f-436c-86db-8ea92f34aaa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f4406c76-12f5-4cba-b65d-965df87cc4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f604647e-3c5d-4ec3-ac43-8cf88d7f832f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "312f81fe-3916-44a6-9240-85cb017db8cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e75496b7-5328-4104-a406-75ee11a16727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 143,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6cc4e9a6-e620-4db7-b322-e5a4bee0ee83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 157,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "17962765-f114-47f2-8f09-ec365dc595d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3cef3696-25cd-48b9-a4c1-01b642be3043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6a95069b-12b9-4733-885a-3f7c6ceebbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 207,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7e9e2f26-1bea-44eb-a41b-56294199474c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 150,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "df4d3ed6-ed6d-4a4a-91eb-5c16d94fde89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 183,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "986e34e4-a57b-4bb6-8346-45bdcf793c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e51c4333-711b-488c-980a-9cc30a6e565f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e574b288-d489-4294-b205-dde5c79c63a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "473a27ee-2d98-4071-b14c-d856da9a6fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3c1a711c-36a6-455e-b8e3-9ce8c9bfc942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4482d24e-be5a-4f4a-8652-1122fec39a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3279db6f-db66-4991-bad8-a28889908e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1082a9ec-b997-4164-babb-ad7bb84dbf7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d4b6ed5f-3ac4-45be-a4ff-f5ff5e1d6ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "19f66ae0-66e9-4add-855e-86c201ef6b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "180a6eee-016e-49c0-abcc-15dc140b06f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5f26f2f1-be40-4b69-ba08-2e41dbec7ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 195,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5fae5a77-8be3-4238-9342-b3b03ce1212d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "679cdb8c-9fd0-41da-9eb3-db3807b8e391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ec65fa0e-90a6-45e6-a252-2f15c5cfcb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fd6cc422-17ff-475e-93e9-62726764c63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "25eb54a8-b4a3-4834-93ed-17ded2b30c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c6ea6ff0-0f87-4451-95d8-ba40fff83ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a976a12e-57fb-46a3-b9a5-718c9940ed53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3ff07611-cf8b-4638-bdf0-bbc65b0a0932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4d4a196b-6244-4315-b4c4-c148397c6669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "77e98ddd-54e2-4253-8885-9afe009e95be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ada1f678-f10c-4b76-ac97-e6bfee9adc5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "79aa894a-4c3a-4945-94bf-e24331fe3921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d684b3b2-0a2e-4cf9-a5a7-9e097dcb9b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6d91828d-a7e3-441d-8436-21a43c310314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 26
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b94c9a02-84ae-4f47-ae90-f07dc50c9307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 233,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "93809fea-55ae-4a32-9d6d-f14354a4c42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 95,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6a6bcebe-4638-4ff8-9060-499979bb09e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "39cc2878-d4aa-4e97-8aa8-19594419ce24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7ae41d74-5ae9-46ea-a089-8c25cbbfa8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "90e176d0-d720-4a7c-99fd-348b3c8d9d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6093ab36-961d-4d42-ae04-c9e2395a2580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "36ab5a57-118d-46a3-8380-7de2817aaa9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d24d1338-5311-487e-b9c1-fa6b6ea62ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "87f2638a-e948-49cd-8f58-28bb3d861d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0535ccd0-d538-4ad9-b0e9-f6bedd0b2858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 164,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "26ad84e2-1a46-4cf8-a065-92ff3d32ba94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 175,
                "y": 26
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e465ee3f-12f7-4a81-aff9-68c180f29926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e5493404-933f-4380-93a8-56bedb588cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "152c461b-4380-40a6-aa22-215e486f640c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "46fcdf4b-13ce-408e-8f56-7badd6d4269d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8f43b653-f490-4876-a67b-43bb688fae3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7c40f1db-2127-4766-821b-919226674a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 241,
                "y": 26
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "00953aa7-75fa-4f0d-adc0-2ee2c1311638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 189,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "73c6394b-b86b-4792-9cbf-c0956a3e7e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "79381fc6-1f5c-4ff5-b804-a65011110350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 164,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5c89169c-3465-419c-a90a-548b1753406c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 153,
                "y": 26
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "61c85a59-6ab2-4a5e-b388-6bcc3e01041d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fa79d1e3-47ef-4482-9206-a96ca0b454e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 127,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a3f39146-81ae-4aad-abe3-c33f2719d5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "eacac09c-c48f-40dc-963d-da1e68c276e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ce757623-16c0-488b-ad9c-52acbbb03ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1a885dad-22ea-4466-93f7-c90c88beb500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 26
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4f5eb101-00cf-493b-8473-70dd96200f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 26
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2c335097-a55a-4468-926c-4af06909721e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5ac944e8-022a-454d-bd58-68f94659daa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b895e014-656b-476a-83d7-cf6ca12fa2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 197,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "96782980-f8b8-4549-b984-99e192e7cc6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 223,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c5ed2323-d986-4d6c-91bd-c89d16ad6cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 111,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "eddefcdf-d749-48a7-8355-c2678d97c302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d6ff7198-7840-436e-ae14-5073532300ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4175154f-4f57-4d15-b3ba-c10c81759485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "27b09796-b6c5-48f4-bee7-7ec09309ccb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "855a7bec-ad4f-4c51-8999-332d0ce30883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8b3c1126-4176-4faf-879b-bad06fc79535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8b2eb554-1222-466f-acdd-e7705a911492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0a8c710c-8e8f-4ce5-8945-e1eead9d68d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0c56a95e-5649-4c6d-8da2-7c5e53454f2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f06b0ce1-1ccd-4dc2-99b0-bf0840fdf7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2046f96e-55d6-46f5-b0ea-2718acbc5c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae42ec02-d66d-402b-90e7-609d17efcb49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bba8037a-8122-4cfd-bce3-14fb39fba28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "29948476-9e02-4b21-bacf-29c56d7f02b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "81099322-d1ba-4971-881c-adf510c556fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ff6109dc-56a1-4378-98f4-3e421e21b5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "989669e2-2f17-4a5a-84ec-c15ca4f389d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "12223329-6ebf-4042-b134-0f4fa8627186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 228,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d651a6a8-d2ca-4d88-a0b2-d80f184bb328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 59,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0751bd5d-7138-45ca-a25a-766745cc7e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 50
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "69a3f80a-e3c5-44a2-a88c-0a68de31d5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "58fbfcba-0486-493c-89f6-a79484dcabce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 99
        },
        {
            "id": "b1ddcbcc-f3c7-4b82-8360-cd20e68213ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "279e32cc-d871-43aa-82a1-d23a3772321a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 101
        },
        {
            "id": "a191d8fb-b449-41a0-b6d7-156be0e89c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 111
        },
        {
            "id": "1005b8a1-0793-465e-89ab-ba6c0b3908d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "151d0cfc-14c7-4857-9d8b-bccbdd8b349d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "90eb4fd9-7ce0-4f7f-ae70-459ad8de86c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "2e014a5d-a7ee-426b-a390-30784ea86178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "64ef2470-706b-4520-9c9a-73be7fee6a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "1617bdf3-bba6-4eb2-8385-ea0e2cbc848b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "b5fa72f9-853f-4a42-a85d-328c8b152f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "6072993e-b81e-4ab1-a12a-9fa67604034c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 224
        },
        {
            "id": "e235f91c-30ac-44e8-be84-384e609b2594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 231
        },
        {
            "id": "1d41f500-e426-4519-8be6-6de4d02e7d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 232
        },
        {
            "id": "a0d159ba-df8c-4288-b255-9c49e2508376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 233
        },
        {
            "id": "d50d52c1-30b0-4d20-bc9b-397b47528ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 234
        },
        {
            "id": "7b37b420-4af5-4df7-a757-9238fbcd3fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 235
        },
        {
            "id": "0f5fee58-d9bb-46b4-bd5f-6590d8c1150e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 242
        },
        {
            "id": "953cba90-d335-4670-9cee-005f1b3b6bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 243
        },
        {
            "id": "9ed45671-fede-4b95-abf4-f5c55020f8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 244
        },
        {
            "id": "9783e466-6d13-455b-8567-617d0218ebd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 245
        },
        {
            "id": "3832c1e8-db9c-4151-8743-feaa3c2495a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 246
        },
        {
            "id": "b3edb491-81d0-4bba-b969-b0b4b09ff0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 248
        },
        {
            "id": "ac98dcf8-a296-4166-9caa-74658b95cbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "e21f8269-9968-4bb8-8f15-92008c73cb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "5245915b-9934-42ac-9b93-e382d4329d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "156a166e-c328-4b25-bb1a-de2ab62a87ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 263
        },
        {
            "id": "f3416943-dc1a-4429-9816-a1e6272dd66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 265
        },
        {
            "id": "36bac3b4-393d-4659-94e8-fb0d71209ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 267
        },
        {
            "id": "4c3362ab-95d4-4be7-a013-6768f5dc5817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 269
        },
        {
            "id": "658a6fa2-1f46-46a5-b0ff-fc5c645e7c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 271
        },
        {
            "id": "086e78c9-ee1a-4083-836d-110baffefc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 273
        },
        {
            "id": "d24e071f-5f1b-463a-a5ed-7c89fb2a4701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 275
        },
        {
            "id": "868eaedf-f387-4560-bb01-4d4d090beefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 277
        },
        {
            "id": "b01b35d3-7b22-44af-978e-ed075e3c5904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 279
        },
        {
            "id": "8628fcce-8367-4edb-86b4-ed8f3bd2f025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 281
        },
        {
            "id": "225293db-5ee5-4ee2-977f-4864e02ee131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 283
        },
        {
            "id": "1f9dbcae-d03c-4af7-821d-f2009a7e7f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 333
        },
        {
            "id": "28b52d10-71d1-46ef-a3b2-b6b61ea74a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 335
        },
        {
            "id": "213a17a0-b46c-435a-9a09-f03faf1098bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 337
        },
        {
            "id": "a9cc05c5-2deb-4cb0-ab3e-e155ac51e60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 339
        },
        {
            "id": "02f33f9c-4de6-491d-88a3-46db47933197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 417
        },
        {
            "id": "90a3c7cf-4bc2-4b5e-b80b-e544611ce8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 506
        },
        {
            "id": "ce877e4e-3ff8-4369-9bc9-ca35b3e009a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 511
        },
        {
            "id": "99a35f37-93b0-4762-81e0-0abf6ff42e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 902
        },
        {
            "id": "e69905bc-52e3-427d-befb-c3cc28c1c3a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 913
        },
        {
            "id": "4ca48f7d-3b9c-45bb-b7cd-9ea9100c12f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "4483b9ac-d91a-4ea9-8cb7-c1d462547e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 923
        },
        {
            "id": "76b8917a-eed3-4876-a0e2-de0536f3cc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1033
        },
        {
            "id": "8dd3e8c5-fb9d-472f-96f6-1f67311d0ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1040
        },
        {
            "id": "4b22b7ce-ba08-4b94-81ad-668d8fd37bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1044
        },
        {
            "id": "3310d433-24d2-4d91-8a10-4cebc6ca2fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1051
        },
        {
            "id": "3eab24b3-ca5b-49df-a443-c0cd359f0659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1076
        },
        {
            "id": "700f2988-54ec-4d85-8d3a-8160f2f6264c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1077
        },
        {
            "id": "29bf6d6e-f9cd-4cc7-83df-1df45cfa8cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1083
        },
        {
            "id": "bf03eaad-8b81-4486-82b6-001d3cc8b3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1086
        },
        {
            "id": "464bf394-ff72-40ef-8f99-468142774a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1089
        },
        {
            "id": "d50924de-bad6-4769-8198-1e62feb8fa6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1092
        },
        {
            "id": "f8349e06-96c0-44a9-ab9e-ebd5ae8cd10d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1104
        },
        {
            "id": "b696c621-df9a-4dca-968f-091e0e8c669c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1105
        },
        {
            "id": "fc9d387e-3cfb-4d35-ae51-58424be65beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1108
        },
        {
            "id": "e6f98268-44de-460e-8a01-346910558d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1113
        },
        {
            "id": "1571614c-64a0-4f18-86f6-399dea630fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1126
        },
        {
            "id": "e3c98d73-c5a9-4299-9270-dc4c45e562ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1127
        },
        {
            "id": "d1bd5774-cb55-4925-bb59-1ddaaf10205e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1139
        },
        {
            "id": "8b47d892-22b4-4cbd-a9af-08ba23959c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1145
        },
        {
            "id": "aa1e02a3-116d-465d-a1fa-62fb2b0268ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1147
        },
        {
            "id": "a0e29ecd-a829-48b5-a553-f771b93f1734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1149
        },
        {
            "id": "3c7936f1-f420-484f-aa74-ae97330f829a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1153
        },
        {
            "id": "562e57f7-5c4a-4284-af2b-9ec2f8795d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1193
        },
        {
            "id": "bb796eb9-532b-45b0-8b05-541a6fc77d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1195
        },
        {
            "id": "d2241a19-e7bd-49da-a397-3ed26fa76024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1221
        },
        {
            "id": "7c26e4a0-26be-4e44-888d-c36578d04796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1222
        },
        {
            "id": "bf244dcf-f513-4b71-8c4a-ffcef2b2f969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1232
        },
        {
            "id": "a11d0d4b-9a79-4084-95f5-5ece3d9eba84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1234
        },
        {
            "id": "b790af00-1b7e-4f80-8361-00d1b15e4274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1236
        },
        {
            "id": "6e73087c-1b8d-45bb-94f2-bb60752a9a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1239
        },
        {
            "id": "5bfa293d-a70f-47ff-b866-f21c1d5d2989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1255
        },
        {
            "id": "0aed7f92-e659-41f2-af66-2559c2208b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1257
        },
        {
            "id": "8b27b028-769d-4cba-aa8a-395531ffd1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1259
        },
        {
            "id": "009f2a6a-bc26-4243-a7f3-d2877e907199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1280
        },
        {
            "id": "4210fca0-801e-43bc-b923-c87cf23a6aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1281
        },
        {
            "id": "ed527ed4-168b-4ce7-a50e-7d2825bcc716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1282
        },
        {
            "id": "668d9de8-5cd7-4109-ba1f-90d0519bb3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1283
        },
        {
            "id": "5c9b283b-54ed-45ce-90c2-619c39b067fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1288
        },
        {
            "id": "f762c37f-0ed5-4004-bd4a-6e0526d65da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1289
        },
        {
            "id": "9afe8e1f-026f-456a-94a8-24e5bfad9208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1293
        },
        {
            "id": "bfd68a95-b9ff-4075-ae26-4303493bc05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1297
        },
        {
            "id": "ca10c268-922e-47ed-b7c1-fc8a73297ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1298
        },
        {
            "id": "5f71f67e-833a-407f-abe6-9ee08707f620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1299
        },
        {
            "id": "5099e34c-5df3-4c06-8a52-1cc00e5ce44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7680
        },
        {
            "id": "c7719b7c-cb05-4469-97ba-14d50af05d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7840
        },
        {
            "id": "88245b51-7ed9-4992-bcf1-63954ceeb336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7842
        },
        {
            "id": "e4478f55-f03e-4079-ad3d-9f101b5967fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7844
        },
        {
            "id": "cd43daab-f08b-4fd1-85f6-9523ad2148ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7846
        },
        {
            "id": "1ad4f17b-3710-4c60-93f7-50a1c9c2e306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7848
        },
        {
            "id": "cabcc679-578d-4a7f-a559-02a004625986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7850
        },
        {
            "id": "11d95ac8-4dbb-4dea-b783-992a57b4efaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7852
        },
        {
            "id": "cbddd19f-23ab-40f7-a649-bb8f86e0d380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7854
        },
        {
            "id": "3554ca5e-3b60-4b98-93a0-f1143e3f1114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7856
        },
        {
            "id": "e87e4a33-9ee0-4e86-bd74-1eaa0200a4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7858
        },
        {
            "id": "38ba40ec-5bac-4ac9-8c91-b4e137959b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7860
        },
        {
            "id": "d148ccbe-dbaf-414d-b8af-16b88df92e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7862
        },
        {
            "id": "e92fc8c0-54ca-482e-b009-7cd03ed36e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7865
        },
        {
            "id": "70fcdd02-30cc-43d6-8c59-15cc3e44bea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7867
        },
        {
            "id": "c41dd904-ee70-4f77-967d-284735a3e584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7869
        },
        {
            "id": "0f238e9a-976a-4315-82e9-4870e06da965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7871
        },
        {
            "id": "c6971548-bfcf-4ea7-ae1e-eb17c08c5920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7875
        },
        {
            "id": "6686d813-b036-4bcf-ba54-8692e460b0ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7877
        },
        {
            "id": "008b5ab2-76b3-4b8a-83a0-08bfeaea4939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7879
        },
        {
            "id": "73a981f6-426c-44c6-8de7-847ab82cab62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7885
        },
        {
            "id": "5b1153e7-f389-4a9c-8468-b98c69ec188e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7887
        },
        {
            "id": "7003e78c-a273-4218-be8a-d857f75d130f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7889
        },
        {
            "id": "73b0de70-5736-46ff-b29b-24d531389bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7893
        },
        {
            "id": "be47f9bd-755a-4367-a061-8fcdd98e5958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7895
        },
        {
            "id": "6355ce2d-3808-485a-af7f-678e0471acc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7897
        },
        {
            "id": "9f8a21a6-6c3d-48aa-8763-397e6beb7927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7899
        },
        {
            "id": "9511a2e9-cfa4-418b-bb29-9205f364922a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7901
        },
        {
            "id": "14685035-fdc4-4f4d-ad81-a7d3602c0998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7903
        },
        {
            "id": "b7777e6e-1c5c-44c1-86be-e1c85e4b5bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7905
        },
        {
            "id": "f6a05722-14b3-43d8-a1e8-153c848271c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7907
        },
        {
            "id": "ad21505e-8475-416c-9698-8b35355ca439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "2b2c4c32-6fbf-4d4a-9de5-135e82fd4ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 99
        },
        {
            "id": "31a5bb96-fd1f-4898-a1fe-e3294c6592da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "26b0f7b7-665e-411d-8b01-fad9a58789e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 101
        },
        {
            "id": "327680bf-5d74-4bfe-a90f-3a94d312ca7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 111
        },
        {
            "id": "29d0059a-891e-47a7-80a1-9c0ff1eea73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "9e94ab40-172a-45df-8eb4-81915ab2509e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "108646a3-622e-4a5c-b7e8-078293b46c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "e4754bd3-b30b-4710-8cd4-adc072b9312a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "1ce200ae-3795-4e56-9ed7-ec804628794b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "cd17e55a-66f2-4efb-9f20-d1f79cb65efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "4f8db03f-9a74-4f94-b62a-ddc689cb1aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "f299dc7a-da46-4fac-9302-a3472c94c91f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 224
        },
        {
            "id": "d2f1037d-9c43-4b44-8d3e-081184d20096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 231
        },
        {
            "id": "6aa0ecec-ac12-43d9-974b-632c44c5b33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 232
        },
        {
            "id": "6eb2cd43-0fe2-4d5a-8c73-3e53388eae2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 233
        },
        {
            "id": "e0e060ee-3834-4827-acc4-6c01dbf32ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 234
        },
        {
            "id": "3b36c089-2991-4330-af20-7c22c9aa667c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 235
        },
        {
            "id": "ec29d409-4b7b-4302-b0a4-1f1fa871b0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 242
        },
        {
            "id": "4af51894-f53c-4070-98b8-fb52eb0600ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 243
        },
        {
            "id": "3ed1a004-e164-48bb-99cc-7132d5a71ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 244
        },
        {
            "id": "dec0400c-e135-4590-ac58-fc1f2e4676c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 245
        },
        {
            "id": "addd0394-d529-43ef-ab11-d85f1437c045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 246
        },
        {
            "id": "7a63dc95-71ba-475b-a38a-ad1ca759f1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 248
        },
        {
            "id": "e4a00faf-4824-4bae-a5d7-ce38aaf103bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "126d5cd8-16e4-4ffc-aaf4-e1ad76430462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 258
        },
        {
            "id": "5dfced50-7ff4-40fc-b8a7-2d298fdaa938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 260
        },
        {
            "id": "b08b8f74-ad76-4efb-bba9-e8d8113a4f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 263
        },
        {
            "id": "56bdbb05-484c-492b-9e6c-49ccf82ae2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 265
        },
        {
            "id": "8a6ab409-2c4f-4f97-b16a-632c7767d4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 267
        },
        {
            "id": "66d6826b-e079-46ad-b77e-fbfee9d347eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 269
        },
        {
            "id": "145c0d12-7f27-4abf-b93b-07e58d3ff900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 271
        },
        {
            "id": "c2ff402c-5518-4135-94fd-5d49e0111e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 273
        },
        {
            "id": "648d46ba-9cd0-4cae-96df-4fc9a2f0925b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 275
        },
        {
            "id": "b7c24bca-0344-41d8-810c-eccd237ead4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 277
        },
        {
            "id": "f17572eb-0049-4994-b69a-39f9774c5eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 279
        },
        {
            "id": "0fa74959-58f1-423c-9b23-ce7d37f00113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 281
        },
        {
            "id": "1f80d53d-0f77-4821-ba87-241d72906cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 283
        },
        {
            "id": "d5f5fd74-b5e8-4d0c-9751-82c05dc4cebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 333
        },
        {
            "id": "f7d4ac7e-17f4-4168-8f0d-ed902a06484d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 335
        },
        {
            "id": "6a409360-ecbc-4072-a007-f7a96ada77b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 337
        },
        {
            "id": "6c1fda10-7881-4428-a7e2-3ae26a20c7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 339
        },
        {
            "id": "dbd9e479-17b1-4c53-87c0-b1d2e2a4910e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 417
        },
        {
            "id": "3aa4dedc-7b06-4f59-bf32-c7b94771bce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 506
        },
        {
            "id": "be3d561e-c51c-4234-9640-f574462307ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 511
        },
        {
            "id": "4d6e07cd-05f6-45c6-84a4-eb50d02c1cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 902
        },
        {
            "id": "63861c0e-d30f-4ed0-b8e4-f77dca36ba98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 913
        },
        {
            "id": "0095b8d2-1a85-4489-9235-4559fce615dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "88bc638f-f5a8-4e30-93ba-bc9dcb5bc884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 923
        },
        {
            "id": "ce1fbec8-983f-47ac-b3fe-334f353e1bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1033
        },
        {
            "id": "21c0035e-bb5b-4bd0-9122-0b7e90a8def1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1040
        },
        {
            "id": "0dd980d5-6a02-4d72-a9dd-11e155aced85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1044
        },
        {
            "id": "8b763d1b-d4de-492e-97b8-a3ff9957d58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1051
        },
        {
            "id": "f4d0bd22-50e4-45d0-be48-71e702c900e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1076
        },
        {
            "id": "90396928-c3e1-4c77-a39a-252a27569589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1077
        },
        {
            "id": "c8ee6bc0-6e72-4066-973c-ac1beb2b2872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1083
        },
        {
            "id": "a7804cee-80f8-4dd9-a732-254399becde5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1086
        },
        {
            "id": "ebd8193b-b49f-485f-b8f0-6936945b7933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1089
        },
        {
            "id": "831ce3d4-9a54-446d-99ef-945fbc1a6549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1092
        },
        {
            "id": "a6c16fdd-ca76-48ba-835f-9aafc0e49934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1104
        },
        {
            "id": "b7c59eea-787c-4149-bd82-01ff81a4b9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1105
        },
        {
            "id": "be05e9a6-d079-4815-845c-780ca41e4662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1108
        },
        {
            "id": "9fe7d979-7bf3-4c5b-b561-4f7b30435fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1113
        },
        {
            "id": "c5ae7a85-e8cc-4882-8147-9625de4256d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1126
        },
        {
            "id": "3bd113a5-ee5f-42c5-87fd-17d64b913ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1127
        },
        {
            "id": "92795868-3bed-486c-87e5-efc9788cc1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1139
        },
        {
            "id": "1127c81c-1a1b-4b10-b42b-0867176e2871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1145
        },
        {
            "id": "25b6cc06-a2bf-4288-ad7b-0362d87ce39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1147
        },
        {
            "id": "c1077d14-d303-44da-90aa-b8ca9d8ce657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1149
        },
        {
            "id": "e2f2247b-5859-472a-a2f4-4b956e8a15bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1153
        },
        {
            "id": "90aad47a-7f08-4c95-b4c0-eb88deda750f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1193
        },
        {
            "id": "afe79996-21f8-455f-8a54-61dd49bdff89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1195
        },
        {
            "id": "e9ceec45-1997-47b2-b3e3-fc6cf27ef391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1221
        },
        {
            "id": "d9cc0a7d-4acb-4e80-966b-37518b21c808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1222
        },
        {
            "id": "3652aade-2303-485c-8e59-2c413e576fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1232
        },
        {
            "id": "e7506537-d664-455e-941d-32540dfb8472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1234
        },
        {
            "id": "d0c7847c-ac06-4e66-89fc-863e1165e508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1236
        },
        {
            "id": "fb89ad60-452b-4dff-9343-5194ad47a55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1239
        },
        {
            "id": "733682d6-07e0-454f-85cc-74726337ebaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1255
        },
        {
            "id": "cee5ca8a-cf22-4aa6-8b4f-17a752f04b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1257
        },
        {
            "id": "99bf3f61-51b7-402a-8d25-f315a997c3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1259
        },
        {
            "id": "79b5be27-5398-4408-8428-40bdbf412eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1280
        },
        {
            "id": "03641373-3d09-4ea8-a8ae-946deee1430f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1281
        },
        {
            "id": "ba36f757-3d4f-4379-babe-d9e558fafff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1282
        },
        {
            "id": "3b0e729a-4db1-42c5-9806-cfbef25ee390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1283
        },
        {
            "id": "8bdc3a04-30a6-4a4d-a0a3-9aa16222497d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1288
        },
        {
            "id": "fc559a5c-3c96-4da1-bbd1-253e85ac3b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1289
        },
        {
            "id": "ae859118-96bc-4bac-998f-c3d713380227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1293
        },
        {
            "id": "949b7550-93ee-4c11-b7b6-b0dddeef2ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1297
        },
        {
            "id": "8154a2e6-d470-40ba-a105-a103568d7dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1298
        },
        {
            "id": "03affd7f-533d-4cda-85f4-700b6dd8721e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1299
        },
        {
            "id": "b90997f3-c6c1-48ed-9082-2b78cd1678f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7680
        },
        {
            "id": "16040beb-f8d3-4693-b57f-327091f42214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7840
        },
        {
            "id": "72efd301-e759-4ffe-b374-8d897c9c6a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7842
        },
        {
            "id": "c8c13b20-f5aa-4d36-bd1c-a73f9f7d8672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7844
        },
        {
            "id": "6221dc60-8eca-4279-9fa7-70c6643387c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7846
        },
        {
            "id": "e38073ab-e55d-4acd-8277-1c455eabd7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7848
        },
        {
            "id": "d2585b83-3230-455c-a0aa-69fe40db31e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7850
        },
        {
            "id": "1723da81-11f0-44d3-9fb8-1734d8f12d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7852
        },
        {
            "id": "cf823ea9-c357-4591-b9e1-b459cc31900f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7854
        },
        {
            "id": "73ac9da5-31a7-4d35-826b-ff39030cfa4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7856
        },
        {
            "id": "803e9853-d9de-46bd-9d1c-d23f8b8a7ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7858
        },
        {
            "id": "94626373-17fe-4041-a206-605923fbe85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7860
        },
        {
            "id": "18eadb9c-07ee-405b-bac2-7070d8ceca95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7862
        },
        {
            "id": "886b9cac-b562-4a1e-bc02-a96cec06e9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7865
        },
        {
            "id": "87c101d5-bbd4-4d17-99ff-5e831217f20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7867
        },
        {
            "id": "597b396e-0653-484f-a057-668c8231eeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7869
        },
        {
            "id": "58eb104a-04c7-489b-aed7-7b554a83cdbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7871
        },
        {
            "id": "f2c75992-f00f-4e86-a11c-c86833779fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7875
        },
        {
            "id": "dd1e4f05-0807-48c9-b7e7-8b06b413aecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7877
        },
        {
            "id": "8611c28f-4635-47b2-9b7a-b2e3e1a5723f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7879
        },
        {
            "id": "9c7bd5b5-ba38-42c1-ae3c-e4592f537a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7885
        },
        {
            "id": "239ad52d-1642-4441-93d1-e5ae4b460bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7887
        },
        {
            "id": "8497e503-0bfd-47dd-af0f-3c3add0d76d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7889
        },
        {
            "id": "10bc24c8-1780-4867-8988-783ce6fe6f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7893
        },
        {
            "id": "2f085e5f-cd78-4568-aab3-f3cc2af72919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7895
        },
        {
            "id": "8522946b-5807-4c9f-8b22-730e2bfe8fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7897
        },
        {
            "id": "af7f3d76-2939-4447-a37b-efb955b2d350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7899
        },
        {
            "id": "0c8a6bc6-c78c-4f99-8b2a-e70773acf02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7901
        },
        {
            "id": "a3ca29f1-42d2-4888-a44b-8589d0c25ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7903
        },
        {
            "id": "ddcc8690-a66a-44a8-bcba-e2a530c00660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7905
        },
        {
            "id": "b77c8f79-1262-40a0-a6a8-d119f6b09f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7907
        },
        {
            "id": "bf58c4ca-40dc-48b5-bbcc-267bc0161ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 74
        },
        {
            "id": "89505259-efa9-4307-b2e8-897e7b9aa4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 67
        },
        {
            "id": "6e1c5aa3-70ad-4a95-8448-9eb4fb28119f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "1abb9e97-213a-4a0c-893e-791ce2fd89e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 79
        },
        {
            "id": "3cbd2e44-cff0-4159-83e7-3b688ab2adbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "f8af8c56-f7f1-443f-891d-81c371b4439f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 84
        },
        {
            "id": "c641e4bf-446f-4c5f-a824-991156a76f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "4ab1d3c2-d010-4472-b1aa-344f31d58f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "a288a2e0-d4b9-4add-8f02-3cef867c8612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "5b480cbf-fc64-498f-8e55-e1f71582da82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 199
        },
        {
            "id": "9bb7149f-9f74-4eb2-96d5-f4092d763a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 210
        },
        {
            "id": "57ca873a-8624-4300-ad8a-000c4203788a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 211
        },
        {
            "id": "4a5989ad-28c7-4127-92d0-0be91115712a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 212
        },
        {
            "id": "22efe9c1-f204-4b29-8daf-af9f1e5c5313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 213
        },
        {
            "id": "95f7181c-e6cb-4ade-a3cf-c2453fd065e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 214
        },
        {
            "id": "115bd8f3-b6ac-4b31-8dfb-1b2498d579a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 216
        },
        {
            "id": "6310760c-d5d7-4b2e-9830-4c011bd31970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 221
        },
        {
            "id": "5db79884-aa85-498c-9e8a-6a11a68ca5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 262
        },
        {
            "id": "e0f71f17-c290-4bd8-a7bb-ebb967d9e73d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 264
        },
        {
            "id": "b6d6a196-fb4b-4391-bac1-c424415b0b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 266
        },
        {
            "id": "cd1f3b45-acee-42f2-b816-8d5830023d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 268
        },
        {
            "id": "a20a5c8e-c785-4699-9d63-8e59db39a0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 284
        },
        {
            "id": "20a4fc78-efdd-4776-960f-6d1a46d6d688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 286
        },
        {
            "id": "99f4bbb3-a366-4f15-9ec5-5ec8ee2405c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 288
        },
        {
            "id": "9ba0d686-be75-473c-a6a9-9ab15adb6c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 290
        },
        {
            "id": "f07dcfd6-0798-4a52-a778-b17fb025b7f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 332
        },
        {
            "id": "717431be-c9af-49a9-9e3c-2ecf0fd67d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 334
        },
        {
            "id": "07cda205-70d2-4f3a-a79c-e9f67622f913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 336
        },
        {
            "id": "884e8e5b-d9a9-4b18-ad1c-6365dd836991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 338
        },
        {
            "id": "38d8ea03-7e32-48dc-a3a4-032c45ffcf96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 354
        },
        {
            "id": "5e57e788-b3ab-403f-b3aa-85a792cd1529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 356
        },
        {
            "id": "d291fb83-a2eb-4ca7-83df-49bcf78bcba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 372
        },
        {
            "id": "a8834cb3-9137-4de2-afeb-3b3acca42e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 374
        },
        {
            "id": "159811a1-1813-4aad-8ebc-e1a4813da470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 376
        },
        {
            "id": "5d6f852b-5992-4e0e-bcdd-14f8eaaf6364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 416
        },
        {
            "id": "8dd67ede-e843-4eca-aadf-bddd7d310bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 510
        },
        {
            "id": "847845c4-839f-4e46-88c3-6f3b88bbac47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 538
        },
        {
            "id": "1d89de57-a0b3-442a-9e95-521e7b5ed346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 932
        },
        {
            "id": "c92d9711-b09e-4fbb-8797-789e69218d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 933
        },
        {
            "id": "eb29e695-d2b1-49bb-a341-05cdc1be6383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 934
        },
        {
            "id": "91c3a75d-59e5-4b64-9024-f81243a435a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 936
        },
        {
            "id": "bd6ea817-35a5-46a2-b674-2ebc61aa1b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 939
        },
        {
            "id": "18fc1dd9-9c94-40b5-9451-b5ee388be8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 978
        },
        {
            "id": "8c0bf276-346f-4373-a489-90ed6f23b3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1026
        },
        {
            "id": "e6af46ae-c941-4ed1-8b29-964448f03926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1028
        },
        {
            "id": "5e3a313a-865b-47ab-add4-28f5dae9f580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1035
        },
        {
            "id": "60d3117b-673d-4832-86ca-a2a3a1f0c98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1054
        },
        {
            "id": "a5b2c5e2-67a6-4a0d-af8f-5e02c2938a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1057
        },
        {
            "id": "7219b329-895b-4a82-a79b-044835f6bc90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1058
        },
        {
            "id": "323e4ece-f5ea-4220-bfdb-a05fae678dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1063
        },
        {
            "id": "f49809a3-bdf2-4936-9892-935044e5bd37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1066
        },
        {
            "id": "3f3ae6ef-6603-4cea-9b3f-6d6853b95b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1090
        },
        {
            "id": "396efb63-c511-4809-8f95-b5afb8aa64ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1095
        },
        {
            "id": "ab0837d4-084c-4b64-9850-50c3dc7fa6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1098
        },
        {
            "id": "c4d1d30a-5c3d-4423-b6fa-13e7e55f1aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1120
        },
        {
            "id": "46275903-7b92-4295-953f-7ed43d54d07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1136
        },
        {
            "id": "d5313920-c26d-428d-a737-c3a8da165717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1138
        },
        {
            "id": "6e6122c4-1176-4b08-96c8-6f75f488119f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1140
        },
        {
            "id": "6a98b976-b286-494e-ad6a-ab4e2a12eded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1142
        },
        {
            "id": "c73b065c-79af-4fba-8647-7669ebfbaf1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1144
        },
        {
            "id": "4a546ae8-5868-4c5a-87f3-4c61612dd55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1146
        },
        {
            "id": "f67aebaf-4c23-4f7b-8ac5-69bcc10e63f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1148
        },
        {
            "id": "ad1e4b8c-77fa-4bef-82c6-97cd48609590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1150
        },
        {
            "id": "ee8462b8-b3ba-4696-9fe3-70662a1746a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1152
        },
        {
            "id": "b1d04614-8081-4029-a4c6-2eab52417c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1184
        },
        {
            "id": "65fbfd8b-5197-4f33-bf06-b10836e9fa4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1185
        },
        {
            "id": "fc1dce7c-6b31-41e9-b020-ce1b398da5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1192
        },
        {
            "id": "dd07922b-a620-4cc9-8278-eb5aabb29713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1194
        },
        {
            "id": "0020af6b-93c3-49d0-ac99-8b79d35dce4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1196
        },
        {
            "id": "0c2f55f0-6814-4148-b8e2-f9c163a0c7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1197
        },
        {
            "id": "2b0274d4-4fe2-4e19-8b2d-646c0a8f3771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1198
        },
        {
            "id": "197cf089-1c3d-4622-a911-441e5fa9e43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1200
        },
        {
            "id": "762365de-8b60-4581-a7c0-a9d45c56e4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1204
        },
        {
            "id": "4c9f4cff-2219-4dc1-90f2-a057814625be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1205
        },
        {
            "id": "820c46ea-fad3-403c-947c-945dbb2900ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1206
        },
        {
            "id": "02fe5669-48a4-4e5e-88bf-6dddf0eeef3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1207
        },
        {
            "id": "a2e09115-71d1-4ef7-87bb-4dd435ef0c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1208
        },
        {
            "id": "94f109cd-72e5-4747-a35a-10427e19ac08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1209
        },
        {
            "id": "382940bc-1555-4459-a37b-3744754683cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1212
        },
        {
            "id": "ad975de6-3b33-4b2b-8208-6a4699037c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1214
        },
        {
            "id": "1b9c1974-dbf4-4634-a47d-d6e3811f7e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1227
        },
        {
            "id": "e7eb7965-b4de-4f62-b95b-07b38d92aebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1228
        },
        {
            "id": "c985ba33-3fd3-4d34-8b98-d3a526633574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1254
        },
        {
            "id": "b8847750-73e1-4eab-b04c-ca7147ce0555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1256
        },
        {
            "id": "ba3c4f3f-fab8-4717-8f32-41d043c15fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1258
        },
        {
            "id": "ce7b975d-751f-4c6b-9fc6-13504fc0073c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1268
        },
        {
            "id": "a2d1daec-c8e4-436c-ab5b-b9798eea50e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1269
        },
        {
            "id": "f987ff97-baac-4d27-85d2-dd39956c561f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1284
        },
        {
            "id": "4a88d2cb-919d-44e4-b0c0-4580218a7d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1285
        },
        {
            "id": "4d5077b6-4d84-410f-ac65-4c3df69cf8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1286
        },
        {
            "id": "36afaad3-9dc9-491b-bc51-eca542f4a6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1287
        },
        {
            "id": "9b838e3a-4ae3-414f-b701-a9216c15c53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1292
        },
        {
            "id": "ba8d9078-a11c-4bf1-b07b-713f13e108b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1294
        },
        {
            "id": "404d2ec8-89f2-4568-81cc-71740b681e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1295
        },
        {
            "id": "102767bc-f265-4f5a-adc6-e917ad0180e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7808
        },
        {
            "id": "2349eed7-017e-4906-98fe-3af45932f736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7810
        },
        {
            "id": "7eb52ddb-66a2-440e-8aed-6f66ca5e0820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7812
        },
        {
            "id": "765da5d2-da1a-45f9-87b0-477ad720bb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7884
        },
        {
            "id": "7881d26e-1c5d-411d-aed7-ba8803450668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7886
        },
        {
            "id": "5c585f6c-f342-4034-820b-c1d676d851ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7888
        },
        {
            "id": "5aea8a4f-0d88-4c88-82e6-98a05eb24533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7890
        },
        {
            "id": "e258dd91-5fd2-410a-be3a-f5f891070260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7892
        },
        {
            "id": "81a1c741-2437-452f-bca3-6e0204e8439e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7894
        },
        {
            "id": "9b3b0f99-e579-4bf3-94b1-0982c49db71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7896
        },
        {
            "id": "01dd298f-f26b-4f0c-85a0-a4bfbc623f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7898
        },
        {
            "id": "49ba0ac9-89f5-4577-b4bd-e39b14194022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7900
        },
        {
            "id": "f0ffdd60-ee88-4140-930d-b6dc80ee3f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7902
        },
        {
            "id": "32a30eb7-5c8f-4a0b-a5ea-d1c769a601f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7904
        },
        {
            "id": "fcdc80f0-a138-4899-a695-a7ba8cf52c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7906
        },
        {
            "id": "699fc6cd-c313-48b1-88e7-a179eac822fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7922
        },
        {
            "id": "3049b18c-4e6e-4748-ab89-7b2915bb2dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7924
        },
        {
            "id": "d6c06544-10a1-42c2-bfda-6ac5249d00d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7926
        },
        {
            "id": "aa4b51bf-6584-44ac-8666-23c5fa3619e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7928
        },
        {
            "id": "6781972e-0397-4a90-99e8-3e91657a4cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "d94f4ef9-82c9-479c-afea-57cb9166e990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "b57289ea-2240-477a-944e-fcebc224f629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "15c7db28-bc24-4998-bc23-de2829bb62da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "50016766-0756-4cdb-be3b-b0e877e3cc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "cb19209b-f4bf-409e-9205-81eec5935e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "2d9115ad-8bf7-42f8-bc7b-771c981deae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "ba6b597c-da53-4c5f-aaa3-8b150851748e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "4a5b9701-2462-4c13-94df-bb7f52adab2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 199
        },
        {
            "id": "39f3df6e-b74b-4b95-a495-150a949aede8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 210
        },
        {
            "id": "89eae984-7573-4286-a664-49e550a15750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 211
        },
        {
            "id": "ca6c1474-8d93-497f-b8b4-d7ddcb20e87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 212
        },
        {
            "id": "025b93c2-eae6-475d-8d8b-ab538474e62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 213
        },
        {
            "id": "d75dbc16-80d7-4fff-a655-dbfec782732f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 214
        },
        {
            "id": "bc80132e-2b65-428f-b3aa-72565e6b9cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 216
        },
        {
            "id": "379f086d-0010-4ed2-adca-29bbbb71634b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "ac8c9933-adeb-4468-b314-0b511e97d797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 262
        },
        {
            "id": "7d4c1ce4-f8e2-40b5-ad1b-8de69bfce29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 264
        },
        {
            "id": "0cdf98fd-dffe-4151-9949-adb3eaf5b8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 266
        },
        {
            "id": "0a315716-c088-446f-96d7-26f133f5359c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 268
        },
        {
            "id": "86e8db74-f745-47a3-a4e5-57bef2261d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 284
        },
        {
            "id": "0d3cb29e-fc6a-4ffe-a056-b35afdc3fb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 286
        },
        {
            "id": "66019d0e-41f5-4deb-a8a1-43f0ded3411b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 288
        },
        {
            "id": "51912ef9-e5fb-4a4c-a7ea-55e20163f0d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 290
        },
        {
            "id": "e884d0f7-59d4-4ab8-8ed9-8288ad3d1ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 332
        },
        {
            "id": "46571576-2bc3-4eaa-b37c-8a01a6bca124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 334
        },
        {
            "id": "d74bd44c-b58b-470f-8bb5-2de0b8971309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 336
        },
        {
            "id": "0e39ea1e-6d8b-47f9-be49-7278a0eb98c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 338
        },
        {
            "id": "957af39b-509c-4394-9f3f-f89737c25613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "5e6bc5da-8653-4493-8ba7-94d0cbc8f2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "be81a9ae-7235-4af1-9813-fb327a48dcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 372
        },
        {
            "id": "ac979f02-a968-44ce-b92e-f95a374d574e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "aeb9a948-b09e-4178-a083-eb3f1b609065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "307acaab-0473-42fc-b977-8dfab8a6143c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 416
        },
        {
            "id": "db25cd3a-da82-421b-b7a8-b2350550747f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 510
        },
        {
            "id": "ee6e1219-71bc-4d7a-aa4f-cd7cb0729c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "ef546706-ad50-4d02-aaf0-d837ae07e854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 932
        },
        {
            "id": "ff5266c1-11ad-418c-871f-0bd729bcba83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 933
        },
        {
            "id": "32dd9b41-5684-43e7-b7e6-d107e21c6869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 934
        },
        {
            "id": "9fd8ebde-b60e-4855-b532-952fee079f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 936
        },
        {
            "id": "f1cce4fb-de0a-4092-bf3a-ed9db7fc9d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 939
        },
        {
            "id": "3ab1e489-a21c-4171-b5fb-145da3282dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 978
        },
        {
            "id": "4437b89d-2e67-47f0-9df9-c3348e2f9cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1026
        },
        {
            "id": "959e91a6-2c7f-4d4b-8a78-901db2e8fc1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1028
        },
        {
            "id": "2ecfd478-e1dd-418b-b65d-a990aebaf9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1035
        },
        {
            "id": "97fcc105-e363-4fa0-9322-b44491aa1eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1054
        },
        {
            "id": "26d905f2-8529-4929-a59a-5340276c7def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1057
        },
        {
            "id": "0d56bf45-b0a0-49d6-8aed-f98ae07e2bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1058
        },
        {
            "id": "f7267022-2171-4c3a-9d9c-86dd55c19b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1063
        },
        {
            "id": "e9b203fd-2b55-486d-a5e3-1eac40d127bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1066
        },
        {
            "id": "0369f69c-25a0-445b-8833-fa1dc8c9f194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1090
        },
        {
            "id": "5c3a22ff-8dd3-4f97-9903-f9e2f2ac43e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1095
        },
        {
            "id": "152416f4-aa74-470c-a40a-4235d22e554f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1098
        },
        {
            "id": "ae275a1b-4ec5-4aea-820d-cc9689f80dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1120
        },
        {
            "id": "7d7ada5d-da59-4227-bd32-dfbeabea72d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1136
        },
        {
            "id": "38f5923c-fc08-4a1a-b309-890e607ba3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1138
        },
        {
            "id": "ee095288-a0cd-4a33-bc36-b97014256958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1140
        },
        {
            "id": "77f0c3f6-b40f-4006-a0e0-9647cdc91191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1142
        },
        {
            "id": "db210e38-5850-4e67-9f85-5004961bf78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1144
        },
        {
            "id": "36faaaae-02fe-4610-99ba-4a6dd8e17854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1146
        },
        {
            "id": "1bf13115-352d-4a34-8679-96c61b3b3628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1148
        },
        {
            "id": "735dcafa-0e95-48bc-962f-e46636c426ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1150
        },
        {
            "id": "4872ad6c-e2b4-4e50-8b44-28d44f491de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1152
        },
        {
            "id": "961b536d-6a75-411e-8647-7a479ac4d0d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1184
        },
        {
            "id": "c3edf053-9090-4372-b874-a916f224cffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1185
        },
        {
            "id": "6e0a9741-4d31-4f33-b954-3b9f6686e3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1192
        },
        {
            "id": "18dbd4cf-f5cc-4509-b28b-9cda647d467a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1194
        },
        {
            "id": "a864c2ac-00fe-4d77-850b-edfa3123d18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1196
        },
        {
            "id": "ea247864-af90-4ba3-957a-375f4f28ba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1197
        },
        {
            "id": "dac69a6b-f142-42cb-9668-8e4ceedbeafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1198
        },
        {
            "id": "51d53307-2737-45c7-bb89-6c4af3aa586e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1200
        },
        {
            "id": "0a28d153-d293-4b6f-9a13-a69242f8ec22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1204
        },
        {
            "id": "079d2454-f2fd-4717-93f9-2843b85c4cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1205
        },
        {
            "id": "a8e87e34-8468-433d-8e70-ca2803480078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1206
        },
        {
            "id": "a47c1020-8486-48b3-b5a8-833b4b290247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1207
        },
        {
            "id": "90da6ea7-f4c9-45f4-9fdc-72ec80ff821b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1208
        },
        {
            "id": "cc5fa6a5-f13f-478b-bb0f-6cb6b44f57d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1209
        },
        {
            "id": "67df4d53-a6c5-47ae-98e6-61e6a1ac1aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1212
        },
        {
            "id": "774e3171-0e6d-443b-88e4-f737b67fb22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1214
        },
        {
            "id": "654e74bb-5b02-4974-8ef0-4cf973e74a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1227
        },
        {
            "id": "6711927b-e67d-4af6-be92-608f699f727e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1228
        },
        {
            "id": "74d480a2-9a3a-4ec5-8e5b-42648b95841e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1254
        },
        {
            "id": "28b537a3-0768-4c48-9a85-b860105f18bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1256
        },
        {
            "id": "1b316cc4-c6e8-45f4-aac2-e67c6db51329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1258
        },
        {
            "id": "18d514ea-917c-4b4c-a8de-5e753af6f0d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1268
        },
        {
            "id": "827d59b9-de08-440f-b9d3-3a89f6e60e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1269
        },
        {
            "id": "c245987d-4d82-454a-a7e5-422f96c8b679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1284
        },
        {
            "id": "adbe33c1-966c-4bbe-9264-406b837e2dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1285
        },
        {
            "id": "9194af7d-4807-44c0-84e5-f5e8eac5f7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1286
        },
        {
            "id": "4254bcc6-13ad-4b39-806b-1ee6dcfca499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1287
        },
        {
            "id": "7c27c034-cdaf-45fe-aff2-607eacc9100b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1292
        },
        {
            "id": "aef0822a-bfa1-45ec-b794-fe0c2ad01281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1294
        },
        {
            "id": "34c77888-75d7-457e-bc0f-8fba42cc89c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1295
        },
        {
            "id": "47fc63f7-5ee7-48e8-a652-ea5fff2e81a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7808
        },
        {
            "id": "6212b337-2253-4909-abde-4c0cea8e9d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7810
        },
        {
            "id": "7227a3b1-9973-42f4-a96e-c08213dbb7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7812
        },
        {
            "id": "e7cdc39e-5937-49b7-82c8-bf6d7ee41a13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7884
        },
        {
            "id": "0bebc7f8-1dea-4119-816f-b0138bb45d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7886
        },
        {
            "id": "1c89b02a-7ca1-4955-9bba-901e9083f7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7888
        },
        {
            "id": "5a93a2c0-1566-4a16-b3dc-34c6866c9a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7890
        },
        {
            "id": "8352f959-ca60-4624-8a03-7dd1cc8932dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7892
        },
        {
            "id": "bd1ff6ac-4413-488a-b03c-7db286b44af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7894
        },
        {
            "id": "d8cadaca-cc79-47d7-ab0f-b1e17bc493f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7896
        },
        {
            "id": "d68598d9-3e84-4652-a3f8-f8b926f60c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7898
        },
        {
            "id": "bc43f5ce-0368-4411-b578-4d2634ff55b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7900
        },
        {
            "id": "78a34419-1cb1-4541-8596-1319e2e0f977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7902
        },
        {
            "id": "061928bc-20e7-4700-9895-fe2991b80e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7904
        },
        {
            "id": "c5d9d1b2-e115-4699-a820-55b97eb9200b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7906
        },
        {
            "id": "dea5cbd3-42a0-474d-93b1-58f4a3469051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "e4a39e7d-0507-4d71-9637-9ed3321e68aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7924
        },
        {
            "id": "ea8002e5-d4ee-4bd9-b786-85388463f73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "8286e555-e908-40e7-81a3-7a6cd00403a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7928
        },
        {
            "id": "32fa13ec-6ea1-4282-82c3-9fdf600e0be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "e08b1fa2-924c-43ea-be9b-28bb77eacb15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "27db77fd-5105-464f-8a46-7c330cf408e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "fbb6933f-27b2-46ce-b9e4-4773534c6a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "c411cbf8-8027-40a7-a23f-dc85bca80023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "0e47ae1d-7d02-4177-8d51-ba13bb568f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "51a88cae-b2c7-425a-a3db-81041982f42b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "e08e8393-2c6d-4c69-ab84-f8437b057934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "581379de-e4b1-4423-8514-c2f4a2a25c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "885aaedc-51c0-45f9-a7aa-6fc84c65b2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "01869e30-8358-4f30-81a4-aa4e64665c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "a0926d13-c141-41a3-881a-3d780120963b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "632d20a6-e11c-4029-9dfb-c4d690d3560a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "85cdf253-b870-4eaa-b42f-133bf423cc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "2774063e-628a-439d-b931-8f9299f10594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "183ff59a-852e-4302-a1a5-e09a2c019b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8f415f8f-1d1d-4a22-bbeb-2bef3b281b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "60126267-49ef-436a-8d91-2ce52a99e10a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "0782416a-f157-4ab7-8359-1498cd003275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "95f23f4a-5e2a-4833-8bb1-255d081ee1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "b864e5fe-886c-49fe-9e6a-c7425d08c45f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "c3567dfe-f16d-418d-9ae7-f751d35645fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "9688b1cc-ea29-4789-8801-c7bb591e3930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "2fe2c36f-8f3f-45a9-b7cd-c87b3756adf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "fb29348f-fb30-4253-9697-ef7031e29aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4cbd1c89-0757-4747-8aad-af983bef6330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "88080ecc-c9e0-4748-b3eb-30a59e8dcfc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "c2545732-9f3c-47c4-b12e-b9bcc5b1cb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1ce49780-f63d-4f9c-87c7-99f30f23d459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "b71998d9-0ab5-42aa-a694-e2b63d910e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "b7f530b1-6dec-4bac-a0dd-05e2deb1a876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "69b36eaf-f302-4224-bc39-b6a3a14375b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "9036d68d-09e3-4ec6-b566-90cd93ae1f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "458242cb-b90b-4142-a1fb-77fbaee2cfc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "46591568-2efb-46e5-9f89-94ba45a4c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "38a2a009-7b17-4623-9edc-ed2f7f85bd7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "01895de2-d1e2-4035-85d1-ca37c9ba771b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "dc248a64-6f62-4f82-bac7-58ea8d04f5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "2f79b513-531e-40bc-9c39-de46166a0fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "83b5860d-38d8-4e2b-8437-d20626e3a93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7680
        },
        {
            "id": "2e9a3cfb-5e2e-45cc-8c42-6362b9be3080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "141fc99c-b902-425d-856c-c116887ad505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "b486e27a-ea3e-4182-995c-f279660ed3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "65392f67-fea7-40f5-85b4-1d3fa1ceffe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "0244a315-87de-4df1-b4b1-17029ffb7170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "af69b1e0-c648-40fc-b7cc-c3909e336120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "41d79126-d3ca-48c4-8a25-7077ffc17996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "fb3f33e7-2212-49d6-9c48-2a28b624d7d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "e6f0f847-48d9-4af3-9ef7-f51896866452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "df4d29aa-186f-4b6f-b867-42b5ec51439c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "5e655750-74a1-45d9-af8e-1a3090f99eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "09dea5c9-c0be-493b-b776-f3a65c4e1bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "cc69f88e-f3ea-43b4-bf26-b1e5656c24d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8218
        },
        {
            "id": "45604fba-2bb5-4bee-98cf-0393cda20122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8222
        },
        {
            "id": "3355d996-5a40-4827-9259-e979a6cffa3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "54b2404a-b61a-42a2-9b96-d457127ae33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "84614c88-aa3c-445b-8ea7-b82987ac96c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "b0e85ea8-368d-42b4-8734-88ea8877fae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "21d4d055-e1a8-463d-a733-a1226ae72cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "97e7f113-3db0-4ce8-b109-1c60865bf716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "ad920537-4bd8-452a-83d5-e0799ae93221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "e67f846e-407e-4fc6-9c4d-70c67e46a340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "d291f774-fd94-435c-a96a-f898a2672d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "0f03bc2f-4124-4334-a7ad-364211e6cc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "63cf8efa-3f93-40af-9de1-c966abe11209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "0cfc9c61-5a6b-418a-9aaf-61daaf900bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "6205da8a-c52e-4a87-9754-78e1e54b5d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "5aeecd44-9436-4c3f-afda-90bcc40d8f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "8bfade93-eb1e-4aba-8028-8e18978b891b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "df336fbb-d026-481a-b20f-8aa56aeb7a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "44e165de-7e53-4ad6-bf12-4414b4dcb90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "746918e2-7217-4b86-83b1-fafed2d16234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "3929c12e-1d21-4d73-a728-4a3d1857c65e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "31601a1f-09f9-41b8-917a-eb7b788da241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "400bc5dc-ab18-4852-937d-2ebd4b2f8675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "b9c967f0-5c45-4a46-bc77-4fd803fa929f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "841f472e-2ea0-4b4f-beaa-90e7c5f9f2f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "5ca1b1a3-f3fb-4280-940e-c7e859c091be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "fc7f6d2d-d2cc-4ca9-834a-6c725c7f62f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "d71b3c9f-39bb-4a76-a2e7-4bb3f7ac5d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "9b90801a-8638-4e90-9c06-b12dea9eef51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "990dca7f-1ff3-4a91-8a08-65c23e562b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "7dd895bd-0c6d-4501-8977-f6e4dd73db98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "6bd83c95-2ca3-4d30-80cb-020447a6480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "5c24f18c-57ad-4e76-a1f5-e1bee0c5df58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "f387cda5-b4d2-40c7-a7d1-b591f4cc7ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "3b149040-e887-4340-a910-83238f7bdfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "1520d048-8a88-43d5-800e-4e528b184ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "0a021534-eb99-4be3-9dc0-4b00f073b8f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "2cc85cda-9093-4fc7-9a07-e54397911f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "0296d858-817f-4eaf-bff5-099ef603740d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "83eb2883-8489-4901-8c82-70df920b82af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "4cae6817-568d-4fd1-b9a0-7290cac8cb00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "ed294da8-ffe2-498c-8835-175aae03e65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "a5bbc75d-1807-4c31-baf9-79520f902ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "5bfcf053-aebb-49aa-bf54-ec93d9110882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "cf130e43-3461-4fd8-9ce8-400f9cf01960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "f74b2ad1-1b57-472e-943b-2103c5ed2e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "0fde244c-2f64-4eef-8350-89abed82c9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "129703fd-91f2-4d97-9431-80aec594f6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "c15bf1ec-5592-4d0f-8b0b-958acedd1b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "ae080814-f74c-42af-a2f3-73c785c36676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "3670c640-41a8-4230-bc16-5cc2771baf77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "05d5e8af-bc5a-4e6b-b20a-f4e425475134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "9b2edf79-0ec0-4aa7-8a9d-8a36ba796e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "f4819605-19d1-4200-9dda-e7c3e4679284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "4bba7128-0a37-44a0-b6d5-3b7a79854b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "c80b9821-9613-46e3-83a6-346e94ab83bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "7072a495-e457-42ce-8aa6-229c7248277a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 271
        },
        {
            "id": "e8c4448a-3b5e-40f9-87bf-cdc369d83ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 273
        },
        {
            "id": "a577d249-edd9-4690-85c5-ecc9142b9202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "617ed36d-98b6-443c-a0dd-9675cbcdd9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "ea8f956c-3f43-4ae6-97dc-ba4588be312b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "92f16dd7-989c-4a23-9a56-504b7a266f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "afe5c60e-e4a1-452c-8f33-f1750098bc9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "8b852dba-fd4f-44ad-815d-7b328f343070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "14d2b7a5-eb1e-4512-b8a0-c996227d083c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "44d74d29-9900-4139-9dd8-1b1fd23d7ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "f296e005-09ad-49ed-b073-5da749b2e587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "b610d396-c0e0-4761-b862-e006bbd345f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "37e62143-ece1-4a6a-b644-e4f7271a83eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "6fce2828-ea23-4fad-83ef-9f4eba06b83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "e282ad1c-b09f-42c4-9059-1e3e30e06873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "5b0cc9c9-d4c8-4d9d-b821-c3a8a9525ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "89b1167f-f051-42af-800b-7639131a4feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "9caf74f7-95c6-4992-a8a6-d37f967fcf08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "620d7fad-95ff-4f64-b0ee-b346f64f36f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "6c5b7a25-a80d-473a-bc02-82b9a49a4be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "e2b362d9-455d-431e-923b-84b43a439cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "edba543d-8e79-49bf-8c84-56dd5dc0b3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "b51bae70-e232-4650-8d42-4e13164349e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "13dbdf7b-6f2b-4465-97ea-ab3a3d832b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "84f0a060-63e9-4917-8b50-5da33dcbfd89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "24c48081-401b-44c8-8582-2a455472905f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "b5b16c95-dc87-4bcb-8cfd-0bcac3838e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "8a93944e-77c9-4be0-ab51-a1ec6fbd3780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "54c1516b-4429-424b-a37b-ef7107eeab17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "347d0d01-f206-4128-aa0f-5cd0b73ed36f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 417
        },
        {
            "id": "2ee9e351-9e62-4a68-a9f6-7e8b36acabc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "7a9d7f5d-58f4-4031-a744-8d03a08c8e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "871bc551-5f28-4730-aaa5-b8b8d6ea22fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "e5fdf7e1-66ca-4bf1-8ab5-d66e33d9933f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "2291e40c-2c5d-42b2-9697-c29a77ec4fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "7e6dbf18-5048-4f38-a1ed-54472dc0d668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "a68238c5-df2c-41cb-b7ad-879ed955b6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7680
        },
        {
            "id": "2a801c7b-5643-4323-8618-32c06dbe76c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7681
        },
        {
            "id": "55cbce6b-fa43-4e6e-9efa-f91bf3b42ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7743
        },
        {
            "id": "eb897492-cb28-4825-8ce0-d96806d11309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "15955533-19b3-4334-abed-f44eeb935e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "671dc27f-215b-4953-8ec4-fbc5575ab1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "4c98b5be-6c33-40f8-b2a7-5ae6bdaf4c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "dc271055-07a2-4cc2-8271-19c59adaf2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "d22c092e-fade-4f0f-b68f-be8026ca5f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "89532fbd-f8ea-482a-bf1d-da8a685240c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "7d79ca9d-d798-44fb-9ae4-6aae8a02ef9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "19d9bfea-bfc2-4fcd-bb21-62076de7766c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "1520de20-f4e4-4966-833e-e06e9cb34220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "f7c86416-714f-4c38-b235-c64db779baca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "73c2e0ba-b73d-48ae-a804-6e7587a4196f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "3d87ac72-9147-49d6-9c82-3b2a7c1bdfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "50e3945b-e8d5-423a-9d53-fb9918f4928a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "35f2f61d-822c-4c66-bd79-bfd0fd49b30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "ea6277cc-ca1e-45b7-9002-b30fed9c8375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "dd1b0a53-2518-43ff-9f82-ed4d44c7ce4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "57acd530-b776-43be-8e34-e355a254b014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "44a6069b-3b3c-4c98-9193-5b1b34975e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "0aa7cb50-52ff-490b-9786-1edb2cbd8432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "12781fcd-99a1-4bf8-8f13-c2efadea7068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "966f586d-7e65-4459-af6a-cbee8976ebed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "598b0d9e-12df-4779-a6d7-b8195a9f639a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "9f9ed790-d3e7-4b76-b4d3-18c04317d045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "57d3b734-9d42-48cd-906d-ff46fb7d66da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "1fdda743-528f-4d23-bd97-d0bf5829ba40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "a347fbf6-0fdd-4c66-ba53-3fe4561e8d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "ce2c971e-7f94-4a92-9042-61bbd0ec3d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "6bd740c9-9e57-49ba-925e-500b817de2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "d7bea1a2-c2a0-4f63-9be0-0e5149dc0799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "039d0792-19df-4844-be10-f25cfc4e6b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7885
        },
        {
            "id": "09cd902e-aef4-441a-be76-0986fabd259b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7887
        },
        {
            "id": "db5d6e2d-3b20-433b-ae8f-4863a8b83f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7889
        },
        {
            "id": "34e17ec7-4141-44a6-a6fb-0f00b44324f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7893
        },
        {
            "id": "c3b905f8-288f-4da5-8bbf-37499add148c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7895
        },
        {
            "id": "bbf5ae46-4a1d-4be5-aefc-eed4f0f67211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7897
        },
        {
            "id": "d3871fd4-0ea4-4814-8091-2819789f2942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7899
        },
        {
            "id": "61aee199-2c09-405e-a9ee-19b92e1f0445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7901
        },
        {
            "id": "09d707b1-27e0-4726-82dd-3335a0f8764b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7903
        },
        {
            "id": "26356ecc-6168-44c3-8c15-ab5439a4ddc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7905
        },
        {
            "id": "d074cd4a-ec3c-4497-977f-419a27c0795f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7907
        },
        {
            "id": "076802fe-7dec-4cd7-a1a0-511b7374aefa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "0cfcb25f-d9b3-4504-8d5d-0215e9c1c85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "38f45035-04a7-4211-b674-4063f03d83af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "7f893178-b272-4426-ac64-f5c958b69dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "550b5d85-b9eb-4200-b2d0-dc0236786dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "16cf282e-d31b-40e4-b92e-9dbaa4ac5a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "288d868a-2422-4c65-8a5e-8df62d35e74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "2d89a5b4-9f65-4690-a90c-305d4d3027c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "8087e53b-e4f0-498a-b75d-62f0c939152e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "2a31804e-e25b-41e5-9b9d-37cda2af18bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "e31d3a31-4256-475e-99d7-3e87e652b734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "e823aa8d-ce5a-45ce-9c3f-a14236102d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "b6d37657-132c-4786-9e8d-95d07666298e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "4119f20b-ae56-4f2e-a38c-c0397c1581fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "06750449-a8e4-4872-aa05-3a98e820703d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "49d8ae20-fd18-4cca-a19d-b386faf25feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "951d9ac4-dad9-4510-bee9-1706cb0c7bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "4e170542-e10c-4bcd-9b49-9e6f481b8ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "3d22c62b-0d06-439a-bba5-1bef10f8a519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "0757ab4c-6153-4840-9f56-4ac1c340f833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "5e1b0516-31a7-496b-8ef0-b6213d4b54c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "4b154476-cfe6-4de2-ae1c-29dea8fe79b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "19c843ee-2957-44a8-b78b-97fb69113a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "2cc8861d-d15a-43f3-802d-75b013884ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "ffcec1e1-ebf9-4b7f-9595-bd1a44396220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "2c7ae409-82b5-458f-85b8-3b6ed7c19683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "731337d4-809c-4897-90c2-aa716a95991c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "4ea87a3d-4114-4c96-9168-32cdf1746e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "2716e9eb-e457-47b5-957e-3ee2eb18d678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "158fcbdb-230b-46ee-a2ad-4cc4f133d210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "6779b17a-d10c-4803-bee4-61a98b6dc6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "5bf889dd-a7fa-46b3-bda5-d03770c434dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "f8ccb911-d001-4354-9710-f4e49e83db8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "1bc0455a-f7fe-4618-828c-45a45383c20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "40332107-cf9a-4b92-bada-a02cae5a644d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "3b762681-5b92-4b98-8aa7-7f4e7d40c615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "838e2e70-7f19-4fee-93a2-f86861253af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "ce988e5d-b572-4917-b069-31b4da2a9abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "1b742f9f-d63b-4661-aed3-5b6475979344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "e25c43c5-9f57-4ce4-8d82-654c1204f5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "0a8b70ea-7f86-46af-aa19-87b54d1561e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "bd130e0c-6c32-4161-94e7-d732c6329e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "0acadea9-39c2-49a1-b6cf-f0896f10888b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "10a3979a-d189-4f95-b587-bf857cf28fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "4b19c45d-7d77-4077-a667-de2a38cf137e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "60510e12-ddbc-4fc1-874d-e2a98b9c4cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "35038de5-caaa-483c-8684-25f2954a3e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "6d78bc4c-db1c-451e-a0f9-c058aa831687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "3c51fc11-b3f4-488f-9d98-caad315af3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "7272ba23-79f8-4761-9cc3-2eef169ba50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "1cae5aca-82d1-45ad-a658-f59c6af9ca36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "0e1491a1-f650-4d18-a83d-fe018e0ccfb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "db5ec1ac-e828-487b-b45a-c958d19b3b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "a3fb4bb1-f6a3-4f42-b0f4-dd9a623d2bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "99149a93-bd23-407b-8b92-8a6598b92070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "d94650a9-54e9-4a78-b335-762d12c6fb53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "e2751706-a53e-4041-8c16-fb543d84ba5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "e8c3080b-11ca-400c-9d78-6a2df84b9603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "393bfa4b-ffa8-460d-a7b7-3f8a76cbf7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "2936202e-99d7-4755-8ac7-aedb28719ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "20db3208-ac2f-4be5-89f2-1e792126958f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "e5cbcbb4-7893-431b-a7e1-5e00bb879d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "b54e587f-f17f-4d46-9991-f13265dc4960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "0444fed4-5ff3-45e1-9b8f-923fbca9567c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "1ca89935-efd7-4e95-be97-90e1da9dec8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "41bf4b98-f626-46b6-bb9f-eaa70b14e7c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "5045fcc5-8b68-45b2-8b2e-f96259b5b64b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "01181cf0-7049-43dd-85c3-04605f28d6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "e3c9270d-55c4-4148-b75a-4a45c53c9e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "768bfc11-0321-4d73-92f4-a027af495a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "cea7b0b0-1470-41d7-a4a9-54fd525e83c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "7fecf70b-37ff-4ed8-b940-b1bcda6d0b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 417
        },
        {
            "id": "10a46ba0-9a92-4263-9f99-c9f391c2ddbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "3e5c4149-3fd7-4f64-a700-a68452b7c476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "9c0ba491-580a-443c-9bea-96c9c08f7a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "60bef4e4-9941-4cb2-97fd-03169102fcee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "4ed8e57b-6b6d-4452-b397-b63ce8eb79b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7680
        },
        {
            "id": "03516178-9f88-4b12-b435-a487889c37ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7681
        },
        {
            "id": "aedd419e-6ffd-4325-b085-10cf285d7c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "23d01c25-588b-4fac-9406-54e0e64c6af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7841
        },
        {
            "id": "3b8e6800-7691-4028-99d3-6ac9ae36c91f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "a00efb5f-191b-4109-8014-2fd9ff0545af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "69d1dc26-2a9a-4b93-8d09-9535bc2bb483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "bec08da2-707d-47ed-ae06-924887dff558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "53656bcc-cf2f-4fd8-ad6c-16a2252fed2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "eae0b256-3a57-4177-bc56-78aa666afc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "f984b2ab-afa1-4a70-a2cc-45eaaf7806bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "8a659e13-4d23-4da8-86ff-acf643ee423b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "2a0163eb-c8ad-4686-bb26-1a0ebb22ee2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "615eb20e-1704-4b4c-95a0-f05407d7602b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "e14912a2-adfe-4b0a-93b3-d00e60ac8916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "14a54c58-7eb3-4c6f-99bf-058280a6a5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "9fa54710-b399-4902-9742-d3beb81e51c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "d99d2f7d-3974-45ee-a0fb-e1fa796495ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "91e0d766-b799-460c-91f0-82d403570a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "c843e0b1-80e1-48c7-999b-0a957dcb30e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "63360977-39c7-4715-a5ee-bd0bc8b4e3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "fafe1242-7823-4fe1-b99d-9af489da9541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "e6183d92-08ce-4c43-8755-44b27cffbfde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "fb617088-8ea6-42ad-87bb-0d1d9ce8edc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "6ded2c61-e1ca-4a17-ac32-ac3be5899276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "3e9ac651-f8c6-408d-bb6c-e3ae50b505d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "b29e5a92-7915-49ac-853d-817e9fc34821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "6eeff37a-14d8-44d1-b47e-9a12aea7da6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "c3a06a39-0a4a-4f10-9a6c-742cfeb768f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "ed4d454f-646a-41d5-af7f-ede5fcfe6c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "5e61d2b1-b8b3-4a10-9514-43aa7a6ed460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "57d2c09d-17f8-4125-b579-80dae187bea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "c49ff360-1a82-4ce7-87ec-64909cb0b8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7885
        },
        {
            "id": "3398900e-ed77-4374-ba0f-4e1b1545ad57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "68d2158d-45a3-4c85-a454-260688c8b387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "f708f33f-7bdf-47f6-9a77-a2f5f40bfa6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "7403401c-7a7b-4535-a2fd-6cb9b094243b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "4aaf937e-1283-4b99-b520-6cae2a9002ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "41581148-86b9-4951-b5d6-458aafb5787b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7899
        },
        {
            "id": "9e374173-fdb2-4fdb-a284-ff39f4c8c788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "700457f2-7d2e-4ef9-9947-81881b209287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "eadf7fc5-4c65-45bd-a596-90ca33455e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "c158c86c-808c-4270-b389-1d5a5b4f2f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7907
        },
        {
            "id": "75d3ce3e-8fbf-4007-81c9-849b988ab18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "ebaf8892-8056-48e7-a152-515d50062f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "0c4e1326-75a0-4202-a7d7-917642eeffbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 74
        },
        {
            "id": "bda4c520-e556-4938-a1db-626e7c1aaa82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "e9eee5b1-64a4-4b88-ac5f-e034e4f8549c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "3ba81495-1340-418b-97e9-9117b8da0a78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "4f73a134-39e6-43b4-befb-68c49b57d98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "b32970fc-6b94-4144-83f5-032a4b42752c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}