{
    "id": "5cfe7279-fcb0-444c-a297-0a924ea7c267",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGangsterAim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 12,
    "bbox_right": 37,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46244fb1-9861-45e1-8769-d90819ff733c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cfe7279-fcb0-444c-a297-0a924ea7c267",
            "compositeImage": {
                "id": "02e7f53d-2f37-4860-8f03-652c01f1998b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46244fb1-9861-45e1-8769-d90819ff733c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e99e61d-4199-4ef2-a76f-db36fef63ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46244fb1-9861-45e1-8769-d90819ff733c",
                    "LayerId": "5d460ac0-ae81-4712-b1e8-312aaa87dc98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d460ac0-ae81-4712-b1e8-312aaa87dc98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cfe7279-fcb0-444c-a297-0a924ea7c267",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}