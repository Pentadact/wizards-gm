{
    "id": "31196e56-9a21-408b-a51a-e32b90826fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWizard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 10,
    "bbox_right": 43,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ce59937-3f49-41f4-bc5a-28407418251a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31196e56-9a21-408b-a51a-e32b90826fb6",
            "compositeImage": {
                "id": "a9302f90-e7b4-4184-874e-65ede8bb24ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce59937-3f49-41f4-bc5a-28407418251a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e7a8d4-376a-4e11-9dba-912bef96132d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce59937-3f49-41f4-bc5a-28407418251a",
                    "LayerId": "7b77a083-6615-46aa-a080-709c8cb80b3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b77a083-6615-46aa-a080-709c8cb80b3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31196e56-9a21-408b-a51a-e32b90826fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}