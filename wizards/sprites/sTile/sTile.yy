{
    "id": "34a90907-1993-4d26-b1e0-08a169baede4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c20b104e-91a8-446c-9925-b9bf9d0def65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a90907-1993-4d26-b1e0-08a169baede4",
            "compositeImage": {
                "id": "0a04929d-4d49-4ab2-9555-bc4a001afbd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20b104e-91a8-446c-9925-b9bf9d0def65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da25bd8e-e45a-40d3-b5a1-b309fb5740a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20b104e-91a8-446c-9925-b9bf9d0def65",
                    "LayerId": "6d9d980f-0d66-4fa9-8619-c9c7f4fcfe90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6d9d980f-0d66-4fa9-8619-c9c7f4fcfe90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34a90907-1993-4d26-b1e0-08a169baede4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}