{
    "id": "db926401-10a7-4297-85e1-cffaebfc4a25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNotAWizard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 10,
    "bbox_right": 43,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c5dbca1-8135-46b6-9cca-662c88c8e71f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db926401-10a7-4297-85e1-cffaebfc4a25",
            "compositeImage": {
                "id": "eeb3d7ec-42bd-45f6-9f1b-25a0215de249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5dbca1-8135-46b6-9cca-662c88c8e71f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8afdb66c-499f-46d6-aba7-3833c570c87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5dbca1-8135-46b6-9cca-662c88c8e71f",
                    "LayerId": "60292f1a-1b23-438b-9520-e93ffcbc6a4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "60292f1a-1b23-438b-9520-e93ffcbc6a4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db926401-10a7-4297-85e1-cffaebfc4a25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}