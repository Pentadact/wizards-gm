{
    "id": "4e038509-f24d-4840-a6ce-3d5cfce700f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGangsterIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 19,
    "bbox_right": 37,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d7b837f-b9d8-4432-b37c-33081210fd27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e038509-f24d-4840-a6ce-3d5cfce700f0",
            "compositeImage": {
                "id": "41b4d59c-49a3-497b-9f6f-7062fc4a24e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7b837f-b9d8-4432-b37c-33081210fd27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "557b29fe-adba-4bd0-98b2-c428fc8c2f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7b837f-b9d8-4432-b37c-33081210fd27",
                    "LayerId": "4165cead-c4f7-436a-8cdd-3acbc80c812c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4165cead-c4f7-436a-8cdd-3acbc80c812c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e038509-f24d-4840-a6ce-3d5cfce700f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}