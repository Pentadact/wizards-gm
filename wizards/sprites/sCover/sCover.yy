{
    "id": "b59769b9-8fcb-459c-a953-ead054deb079",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7697f19-254a-4cdb-bb4e-2c7a86cf171d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b59769b9-8fcb-459c-a953-ead054deb079",
            "compositeImage": {
                "id": "28140b7e-7eaf-4db9-938c-0cdc46bbc49c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7697f19-254a-4cdb-bb4e-2c7a86cf171d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85855400-a5d0-411c-af55-d97c661f015f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7697f19-254a-4cdb-bb4e-2c7a86cf171d",
                    "LayerId": "dfa59224-37cc-4d42-a8ce-3eacf89dd6b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dfa59224-37cc-4d42-a8ce-3eacf89dd6b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b59769b9-8fcb-459c-a953-ead054deb079",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}