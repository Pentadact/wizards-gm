{
    "id": "c5df048b-4124-4157-91e2-284876558017",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelectionSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3cf5af6-9e76-413d-878a-6e3e5b8aa719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5df048b-4124-4157-91e2-284876558017",
            "compositeImage": {
                "id": "994f253b-ea4f-44f5-9c73-cf74d885f4bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cf5af6-9e76-413d-878a-6e3e5b8aa719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04527aba-369b-437e-b0a7-b24c165ed1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cf5af6-9e76-413d-878a-6e3e5b8aa719",
                    "LayerId": "c58391a9-3456-486c-99ba-fc8556c85b49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c58391a9-3456-486c-99ba-fc8556c85b49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5df048b-4124-4157-91e2-284876558017",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}