event_inherited()


var chargeSpeed = 500;
if instance_exists(attackTarget) {
	
	DebugLog("attacktarget exists in wizard step")
	speed = chargeSpeed / room_speed
	direction = DirectionFromAndTo(id,attackTarget)
	if DistanceBetween(id,attackTarget) < 32 {
		PersonKnocksPersonBack(id, attackTarget, 2)
		attackTarget = noone
	}
}

//sprite_index = sNotAWizard

if actionAvailable {
	image_alpha = 1
} else {
	image_alpha = 0.3
}