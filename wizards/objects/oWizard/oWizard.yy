{
    "id": "4127682d-76ce-4732-a3cf-77342b105b9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWizard",
    "eventList": [
        {
            "id": "1958ea3b-4ae2-41da-825b-2e1b138579c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4127682d-76ce-4732-a3cf-77342b105b9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef4a1e97-422c-460c-8417-9bd92b6c47ef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "31196e56-9a21-408b-a51a-e32b90826fb6",
    "visible": true
}