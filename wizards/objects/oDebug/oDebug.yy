{
    "id": "93536e74-3321-4a26-ade0-33e2ea356013",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDebug",
    "eventList": [
        {
            "id": "197d5e60-59fc-4d11-9a12-e4edb4cf003d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93536e74-3321-4a26-ade0-33e2ea356013"
        },
        {
            "id": "7191731e-3818-4c03-8caf-85264a7c02ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "93536e74-3321-4a26-ade0-33e2ea356013"
        },
        {
            "id": "2b65d204-efd0-47d7-887b-1b18ca2025ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "93536e74-3321-4a26-ade0-33e2ea356013"
        },
        {
            "id": "a013021e-d4ab-4d7d-af02-5187f9d85049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 71,
            "eventtype": 9,
            "m_owner": "93536e74-3321-4a26-ade0-33e2ea356013"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}