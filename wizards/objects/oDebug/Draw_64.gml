if shown {
	var lineSeparation = 20;
	draw_set_font(fDefault)
	draw_set_colour(c_white)
	draw_set_alpha(1)
	
	for (var i = 0; i < logCount; i++) {
		var thisIndex = logCount - (i +1);
		draw_text(20,view_hport - (i * lineSeparation), log[i])
	}
}