image_blend = c_white;
if instance_exists(selectedUnit) {
	draw_sprite(sSelectionSquare,0,selectedUnit.x,selectedUnit.y)
	if aiming { 
		var xDifference = gridX - selectedUnit.gridX;
		var yDifference = gridY - selectedUnit.gridY;
		distance = max(abs(xDifference),abs(yDifference));
		
		for (var i = 1; i <= distance; i++) {
			var thisX = selectedUnit.gridX + (i * (xDifference / distance));
			var thisY = selectedUnit.gridY + (i * (yDifference / distance));
			draw_sprite_ext(sSelectionSquare,0,GridToX(thisX),GridToY(thisY),image_xscale,image_yscale,image_angle,image_blend,image_alpha)
			
		}
	} else { 
		image_blend = c_red
		draw_sprite_ext(sSelectionSquare,0,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha)
	}
} else {
	draw_sprite_ext(sSelectionSquare,0,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha)
}