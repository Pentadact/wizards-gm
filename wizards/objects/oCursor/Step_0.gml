gridX = XToGrid(mouse_x)
gridY = YToGrid(mouse_y)
positionValid = false
cursorTarget = noone
aiming = false
//Convert it back to get rounded xy
SetXYFromGrid()


//Selection
if mouse_check_button_pressed(mb_left) {
		
	selectedUnit = instance_nearest(mouse_x,mouse_y,oWizard)
	if point_distance(mouse_x,mouse_y,selectedUnit.x,selectedUnit.y) > 64 {
		selectedUnit = noone
	}
		
}
	
//Actions
if instance_exists(selectedUnit) {
	
	if selectedUnit.actionAvailable {
		
		if selectedUnit.moveAvailable {
			positionValid = true
		} else {
			aiming = true
			positionValid = true
			var xDifference = gridX - selectedUnit.gridX;
			var yDifference = gridY - selectedUnit.gridY;
			var distance = round(point_distance(gridX,gridY,selectedUnit.gridX,selectedUnit.gridY));
			var angleToCursor = point_direction(selectedUnit.gridX,selectedUnit.gridY,gridX,gridY);
			var roundedAngle = 45 * round(angleToCursor / 45);
			gridX = selectedUnit.gridX + lengthdir_x(distance,roundedAngle);
			gridY = selectedUnit.gridY + lengthdir_y(distance,roundedAngle);
			
			cursorTarget = ObjectAtXY(mouse_x,mouse_y,oEnemy)
		/*
			if instance_exists(attackTarget) {
				var xDifference = gridX - selectedUnit.gridX;
				var yDifference = gridY - selectedUnit.gridY;
		
				if xDifference == yDifference
				or xDifference == 0
				or yDifference == 0 {
				}
			}
			*/
			
		}
		
		if positionValid and mouse_check_button_pressed(mb_right) {
	
			if aiming {
				if instance_exists(cursorTarget) {
					DebugLog("attack target exists incurosr check, it's " + object_get_name(cursorTarget.object_index))
					with selectedUnit { WizardChargeAt(oCursor.cursorTarget) }
				}
			} else {
				with selectedUnit { WizardMoveTo(XToGrid(oCursor.x),YToGrid(oCursor.y)) }
			}
			
		
		}
		
	}
	
}