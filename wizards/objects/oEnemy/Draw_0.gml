sprite_index = sGangsterIdle
if instance_exists(aimAtTarget) {
	sprite_index = sGangsterAim
	if UnitIsInteractible(id) 
	and UnitIsInteractible(aimAtTarget) 
	and (ClearLineBetweenObjects(id,aimAtTarget)) {
		draw_set_alpha(1)
		draw_set_colour(c_red)
		draw_line(x,y,aimAtTarget.x,aimAtTarget.y)
	}
}
draw_self()