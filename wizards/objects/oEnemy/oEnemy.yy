{
    "id": "92e5dd6e-5fbb-4147-9b4c-9d881b656976",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy",
    "eventList": [
        {
            "id": "38e38fc4-3337-4694-a792-b95515e13f43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92e5dd6e-5fbb-4147-9b4c-9d881b656976"
        },
        {
            "id": "980c898f-795f-4524-92b4-3fcf68c0739c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "92e5dd6e-5fbb-4147-9b4c-9d881b656976"
        },
        {
            "id": "730ad299-a0dd-43b0-b0d8-9742685f0f39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92e5dd6e-5fbb-4147-9b4c-9d881b656976"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef4a1e97-422c-460c-8417-9bd92b6c47ef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5cfe7279-fcb0-444c-a297-0a924ea7c267",
    "visible": true
}