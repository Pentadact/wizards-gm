if (flying) {
	gridX = XToGrid(x)
	gridY = YToGrid(y)
	if (squaresToMove < 1) {
		squaresToMove = 0
		speed = 0
		flying = false
		MoveToGridCoords(gridX,gridY)
	} else {
		var nextX = x + lengthdir_x(speed,direction);
		var nextY = y + lengthdir_y(speed,direction);
		var nextGridX = XToGrid(nextX);
		var nextGridY = YToGrid(nextY);
		if (nextGridX != gridX or nextGridY != gridY) {
			//Changing cells
			var unitAlreadyThere = ObjectAtGridXY(nextGridX,nextGridY,oUnit);
			if instance_exists(unitAlreadyThere) {
				//unit already there
				//PersonKnocksPersonBack(id, unitAlreadyThere)
				MoveToGridCoords(gridX,gridY)
				speed = 0
				squaresToMove = 0
				flying = false
				KnockPersonDown(id)
				KnockPersonDown(unitAlreadyThere)
			} else {
				if (GridSquareIsClear(nextGridX,nextGridY)) {
					squaresToMove--	
				} else {
					var canMoveX = GridSquareIsClear(nextGridX,gridY);
					var canMoveY = GridSquareIsClear(gridX,nextGridY);
					var stop = false;
					if (canMoveX and canMoveY) {
						stop = true
					} else if (canMoveX) {
						//MoveToGridCoords(nextGridX,gridY)
						direction = point_direction(gridX,gridY,nextGridX,gridY)
					} else if (canMoveY) {
						//MoveToGridCoords(gridX,nextGridY)
						direction = point_direction(gridX,gridY,gridX,nextGridY)
					} else {
						stop = true
					}
					if (stop) {
						MoveToGridCoords(gridX,gridY)
						speed = 0
						flying = false
						KnockPersonDown(id)
					} else {
						squaresToMove--	
					}
				}
			}
		}
	}
}