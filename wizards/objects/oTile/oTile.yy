{
    "id": "681665df-568c-4e24-9303-d36fab0a5d94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTile",
    "eventList": [
        {
            "id": "3f730292-da89-41de-b3a2-eb1f5466864a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "681665df-568c-4e24-9303-d36fab0a5d94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "34a90907-1993-4d26-b1e0-08a169baede4",
    "visible": true
}