
var anythingChanged = false;

if keyboard_check(vk_left) {
	x += 1
	anythingChanged = true
}
if keyboard_check(vk_right) {
	x -= 1
	anythingChanged = true
}
if keyboard_check(vk_up) {
	y += 1
	anythingChanged = true
}
if keyboard_check(vk_down) {
	y -= 1
	anythingChanged = true
}

/*
if keyboard_check(vk_enter) {
	show_message(string(x) + "," + string(y))
}
*/

if anythingChanged {
	with oTile { instance_destroy() }
	with oWizard { instance_destroy() }
	with oEnemy { instance_destroy() }
	CreateTileGrid()
}