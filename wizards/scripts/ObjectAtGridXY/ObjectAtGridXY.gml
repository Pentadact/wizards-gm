///ObjectAtXY(thisGridX,thisGridY,objectytype)
var thisGridX = (argument0);
var thisGridY = (argument1);
var objectType = argument2;
var Result = noone;
with objectType {
	if gridX == thisGridX and gridY == thisGridY { Result = id }
}
return Result
