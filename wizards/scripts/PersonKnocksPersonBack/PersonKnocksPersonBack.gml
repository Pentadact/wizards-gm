/// @param knockerBack
/// @param victim
/// @param distanceinsquares
var knockerBack = argument0;
var victim = argument1;
var distanceInSquares = argument2;
with knockerBack {
	victim.direction = direction
	victim.speed = speed
	victim.flying = true
	victim.squaresToMove = distanceInSquares
	MoveToGridCoords(victim.gridX,victim.gridY)
}