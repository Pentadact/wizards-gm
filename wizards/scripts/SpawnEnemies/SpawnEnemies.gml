
spacesLeftToPlaceEnemies = oGrid.gridWidth * oGrid.gridHeight
enemiesToPlace = 1

for (var i = 0; i < gridWidth; i++) {
	for (var j = 0; j < gridHeight; j++) {
		spacesLeftToPlaceEnemies = max(1,spacesLeftToPlaceEnemies)
		if (
			ObjectAtGridXY(i,j,oWizard) == noone and
			ObjectAtGridXY(i,j,oCover) == noone and
			random(1) < (enemiesToPlace / spacesLeftToPlaceEnemies)
		) {
			with instance_create_depth(0,0,0,oEnemy) {
				MoveToGridCoords(i,j)
			}
			enemiesToPlace--
		}
		spacesLeftToPlaceEnemies--
	}
}


