
with oEnemy {
	
	if (UnitIsInteractible(id)) {
		
		//Execute any possible shots
		if (instance_exists(aimAtTarget)) {
			if (ClearLineBetweenObjects(id, aimAtTarget) and UnitIsInteractible(aimAtTarget)) {
				//Clear line of sight, shoot!
				aimAtTarget.speed = 40
				aimAtTarget.direction = DirectionFromAndTo(id,aimAtTarget)
				aimAtTarget.flying = true
			} else {
				//Had a target but it's invalid - reacquire, we don't shoot this utrn	
				aimAtTarget = instance_nearest(x,y,oWizard)	
			}
		}
		
		
		if (flying == false) {
			
			//If we don't already have an aim target, move
			if (instance_exists(aimAtTarget) == false) {
				possibleDestinationCount = 0
				AddPossibleDestinationIfClear(gridX,gridY + 1)
				AddPossibleDestinationIfClear(gridX + 1,gridY)
				AddPossibleDestinationIfClear(gridX,gridY - 1)
				AddPossibleDestinationIfClear(gridX - 1,gridY)
				if (possibleDestinationCount > 0) {
					var randomDestinationIndex = irandom_range(0, possibleDestinationCount - 1);
					MoveToGridCoords(
						possibleDestinationsX[randomDestinationIndex],
						possibleDestinationsY[randomDestinationIndex]
					)
				}
				//Only re-acquire if we moved.
				aimAtTarget = instance_nearest(x,y,oWizard)	
			}
			
			if (instance_exists(aimAtTarget)) {
				if (ClearLineBetweenObjects(id,aimAtTarget) == false or UnitIsInteractible(aimAtTarget) == false) {
					aimAtTarget = noone
				}
			}
		}
	
	}
}
playerTurn = true
with oWizard {
	actionAvailable = true
	moveAvailable = true
}