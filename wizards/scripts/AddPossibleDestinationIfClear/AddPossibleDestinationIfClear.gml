var thisGridX = argument0;
var thisGridY = argument1;
if (GridSquareIsClear(thisGridX,thisGridY) and GridSquareHasFloor(thisGridX,thisGridY)) {
	possibleDestinationsX[possibleDestinationCount] = thisGridX
	possibleDestinationsY[possibleDestinationCount] = thisGridY
	possibleDestinationCount++
}