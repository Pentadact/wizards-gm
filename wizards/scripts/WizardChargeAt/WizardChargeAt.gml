if actionAvailable {
	attackTarget = argument0
	DebugLog("attacktarget set to " + object_get_name(attackTarget.object_index))
	actionAvailable = false
}