///WizardMoveTo(gridX,gridY)
if moveAvailable {
	MoveToGridCoords(argument0,argument1)
	attackTarget = noone
	DebugLog("attack target set to noone by move script")
	moveAvailable = false
}