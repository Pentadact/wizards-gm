
var thisGridX = (argument0);
var thisGridY = (argument1);
var Result = false;

if (
	ObjectAtGridXY(thisGridX,thisGridY,oCover) == noone and
	ObjectAtGridXY(thisGridX,thisGridY,oUnit) == noone
) {
	Result = true
}
return Result
