///ObjectAtXY(x,y,objectytype)
return ObjectAtGridXY(
	XToGrid(argument0),
	YToGrid(argument1),
	argument2
)