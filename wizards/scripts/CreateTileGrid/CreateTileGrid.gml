
gridWidth = 10;
gridHeight = 10;
grid = ds_grid_create(gridWidth,gridHeight)

pixelsPerCell = 64;
wizardCount = 1;
coverChance = 0;//0.2;

depth = 10;
global.CoverDepth = 5
for (var i = 0; i < gridWidth; i++) {
	for (var j = 0; j < gridHeight; j++) {
		with instance_create_depth(0,0,depth,oTile) {
			MoveToGridCoords(i,j)
		}
		if (i >= 3 and i < 3 + wizardCount and j == 0) {
			with instance_create_depth(0,0,0,oWizard) {
				MoveToGridCoords(i,j)
			}
		} else if random(1) < coverChance {
			with instance_create_depth(0,0,global.CoverDepth,oCover) {
				MoveToGridCoords(i,j)
			}
		}
	}
}


